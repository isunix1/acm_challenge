/*
 * =====================================================================================
 *
 *       Filename:  1.1.c
 *
 *    Description: 从n元素的口袋中抽取4次，看他们的和是否可以等于给定的数m. 1<=m<=50; 1<=n<=pow(10, 8),  1<=k(i)<=pow(10, 8),
 *
 *        Version:  1.0
 *        Created:  06/21/2014 17:29:46
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (Steven Sun),
 *   Organization:
 *
 * =====================================================================================
 */
#include <stdio.h>

typedef int bool;
#define TRUE 1
#define FALSE 0

const int MAX_N = 50;

int main(){
  int n, m, k[MAX_N];
  //read from standard input
  scanf("%d %d", &n, &m);
  for (int i = 0; i < n; i++) {
    scanf("%d", &k[i]);
  }

  //a flag to note if we can find a combination which has the sum of m

  bool f = FALSE;

  //list all solutions through 4 times loop(四重循环), 不知道怎么用英文表达
  for (int a = 0; a < n; a++) {
    for (int b = 0; b < n; b++) {
      for (int c = 0; c < n; c++) {
        for (int d = 0; d < n; d++) {
          if (k[a] + k[b] + k[c] + k[d] == m){
            f = TRUE;
          }
        }
    }
  }
}

//print to standard output;
if (f) puts("yes");
else puts("no");
return 0;
}

